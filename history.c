#include "history.h"
#include "global.h"
/*******************GLOBAL VARS*********************/
char *history[HISTORY_SIZE];
int leastRecent = 0; /* Index of the Most recent history command */
int mostRecent = 0; /* Index of the Most recent history command */
int fullHistorySize = 0;

/*
 * Loads history from the history.txt file,
 * which is located in the project directory
 */
void loadHistory() {
    FILE *history_file = fopen("history.txt", "rt");
    char line[MAX_LINE];
    char *loadedHistory[MAX_LINE];
    fgets(line, MAX_LINE, history_file);
    int i = 0;
    if(line != NULL && strcmp("\n", line) != 0) {
        loadedHistory[i++] = line;
    }

    if(history_file == NULL) {
        printf("Error opening history file!\n");
        return;
    }
    while(fgets(line, MAX_LINE, history_file) != NULL) {
        if(strcmp(line, "\n") != 0)
            loadedHistory[i++] = strdup(line);
    }
    /*Now record the loaded history from most recent to least recent*/
    int j = 0; // note that i now  holds number of the loaded history
    for (j = i - 1; j >= 0 ; --j) {
        recordHistory(loadedHistory[j]);
    }
    fclose(history_file);
}

void saveHistory() {
    FILE *histroy_file = fopen("history.txt", "w");
    if (histroy_file == NULL) {
        printf("Error opening file!\n");
        return;
    }
    printHistory(histroy_file);
}

void recordHistory(char * command) {
    /* There are two cases when the array is full,
     * and when it's not full..
     * each has its own behavior in history recording.
     */
    if(fullHistorySize != HISTORY_SIZE) { /*history isn't full*/
        history[fullHistorySize] = strdup(command); // makes malloc to history[i]
        mostRecent = fullHistorySize;
        fullHistorySize++;
    } else { /*history is full*/
        mostRecent = leastRecent;
        history[leastRecent] = strdup(command);
        leastRecent++;
        if(leastRecent == HISTORY_SIZE)
            leastRecent = 0;
    }
}

void printHistory(FILE *file) {
    if(fullHistorySize != HISTORY_SIZE) { /* History isn't full*/
        printInOrder(file);
    } else { /* History is full*/
        if(leastRecent > mostRecent) {
            printMostLeast(file);
        } else {
            printInOrder(file);
        }
    }
    if(file != stdout)
        fclose(file);
}

void printMostLeast(FILE *file) {
    int i, counter;
    i = mostRecent;
    counter = fullHistorySize - 1;
    while(i >= 0) {
        if(file == stdout) {
            printf("%d ", counter);
        }
        fprintf(file,"%s\n", history[i]);
        fflush(file);
        i--;counter--;
    }
    i = HISTORY_SIZE - 1;
    while(i >= leastRecent) {
        if(file == stdout) {
            printf("%d ", counter);
        }
        fprintf(file,"%s\n", history[i]);
        fflush(file);
        i--;counter--;
    }
}

void printInOrder(FILE *file) {
    int i, counter;
    i = fullHistorySize - 1;
    counter = fullHistorySize - 1;
    while(i >= 0) {
        if(file == stdout) {
            printf("%d ", counter);
        }
        fprintf(file,"%s\n", history[i]);
        fflush(file);
        i--;counter--;
    }
}
