//
// Created by basma on 13/10/16.
//

#ifndef SHELL_SCRIPT_SHELL_LIB_H
#define SHELL_SCRIPT_SHELL_LIB_H
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <wait.h>
#include "parser.h"
                /**Constants**/
#define MAX_LINE 80 /* The maximum length command */
#define PATHS_SIZE 50 /*Assumming that the maximum paths size is 50 */
            /****************************/
            /* Declaration of Functions */
            /****************************/
void forkProcess(char*args[], int background, int pathsSize, int argsSize, char *splitedPaths[]);
char* concat(char *s1, char *s2);
void startShell(int argc, char *argv[]);
int checkSpecialCommands(char *args[], int argsSize);
void handle_sigchld(int sig);
#endif //SHELL_SCRIPT_SHELL_LIB_H
