//
// Created by basma on 13/10/16.
//

#ifndef SHELL_SCRIPT_HISTORY_H
#define SHELL_SCRIPT_HISTORY_H
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
/*******************CONSTANTS*********************/
#define MAX_LINE 80 /* The maximum length command */
#define HISTORY_SIZE 10 /*The max history allowed */
            /****************************/
            /* Declaration of Functions */
            /****************************/
void loadHistory();
void saveHistory();
void recordHistory(char *command);
void printHistory(FILE *file);
void printInOrder(FILE *file);
void printMostLeast(FILE *file);
/***************************************************/

#endif //SHELL_SCRIPT_HISTORY_H
