#include "parser.h"
#include "history.h"
#include "global.h"
#include <ctype.h>
/**
 *
 * @param args put the splitted arguments in it (by reference)
 * @param file File == NULL if it's shell mode, FILE == Batch file which contains commands(in file mode)
 * @return -1 if reached end of file, or some error happened.
 *          0 if the command was a new line
 *          Arguments size otherwise
 */
int parseInput(char *args[], FILE *file) {
    char command[MAX_LINE];
    if(file != NULL) { /* File mode*/
        if(fgets(command, sizeof(command), file) == NULL)
            return -1; // Reached end of file
        printf("\n** %s",command);
    } else { /*Shell mode*/
        fgets(command, sizeof(command), stdin);
        if(command == NULL) { // ctrl +D signal
            return -1;
        }
    }

    if(checkSize(command) == 0) {
        printf("Too Long Command, Maximum chars allowed is 80\n");
        return 0;
    }
    else if(strcmp("\n", command) == 0) {//new line (user entered empty command)
        return 0;
    }
    if(command[0] == '!') {// Don't save "!" in history
        return retrieveHistory(command, args);
    } else { // Normal command
        recordHistory(command);
    }
    return split(command, args, " \n\t\r");
}
/**
 * Checks the correctness of the "!" (command) and retrieves the wanted one in "command"
 * @param command the command entered by the user
 * @return -1 if there is error, args size of the retrieved command otherwise
 */
int retrieveHistory(char command[], char *args[]){
    if(command[1] == '!') {
        if(fullHistorySize == 0) {
            printf("Error no history stored before\n");
            return -1;
        }
        printf("\nMost recent history: %s\n", history[mostRecent]);
        recordHistory(history[mostRecent]);
        return split(history[mostRecent], args, " \n\t\r");
    }
    else if(isdigit(command[1]) && (command[1] - '0' <= 9 || command[1] - '0' >= 0) ) {
        if(fullHistorySize == 0) {
            printf("\nError no history stored before");
            return -1;
        }
        printf("The command number %d is \"%s\"\n", command[1] - '0',history[command[1] - '0']);
        recordHistory(history[command[1] - '0']);
        return split(history[command[1] - '0'], args, " \n\t\r");
    }

    else {
        printf("Command error!\nTo retrieve most recent history type!!\nTo Retrieve a specific command history !Num_of_command_in_history_from 0 to 9");
        return -1;
    }
}
/**
 *Checks the size of the command
 * returns 0 if error size
 * 1 if suitable size i.e <= 80
 */
int checkSize(char *command) {
    int i = 0;
    while(command[i] != '\n') {
        ++i;
        if(i >= MAX_LINE)
            return 0; // Too long command
    }
    return 1;
}

/**
 *
 * @param string the string that will be splited into tokens
 * @param tokensArray the array that will be holding the tokens
 * @param delim the delimiter that we want to split "string" on it.
 * @return the number of splitted tokens.
 */
int split(char *string, char *tokensArray[], char delim[]) {
    int i = 0;
    char *token_ = strtok(string, delim);
    tokensArray[0] = strdup(token_);
    /** need to check the Command Size... **/
    while(token_) {
        i++;
        token_ = strtok(NULL, delim);
        if(token_ != NULL )
            tokensArray[i] = strdup(token_);
        else
            tokensArray[i] = NULL;
    }
    return i; /*i hold the size of the splited elements*/
}
/**
 * @param splitedPaths is the array that will be storing the ENV PATHS after splitting them.
 * @return the number of splitted paths.
 */
int findSplitedPaths (char *splitedPaths[]) {
    char *paths = getenv("PATH");
    /* Split paths over ":" */
    char token[2] = ":";
    return split(paths, splitedPaths, token);
}
