#include "shell_lib.h"
#include "history.h"
void startShell(int argc, char *argv[]){
    char *splitedPaths[PATHS_SIZE];
    char *args[MAX_LINE/2 + 1]; /* command line arguments */
    loadHistory();
    int should_run = 1; /* flag to determine when to exit program */
    int background = 0; /*flag to determine background or foreground*/
    int pathsSize = findSplitedPaths(splitedPaths);
    FILE *inputFile;
    int fileMode = 0;
    int argsSize;
    if(argc > 1) { // read from file
        inputFile = fopen(argv[1], "r");
        if(inputFile == NULL){
            printf("Error opening file, file maybe not found.\n");
            exit(0);
        }
        fileMode = 1;
    }
    while (should_run) {
        switch (fileMode) {
            case (1) :
                argsSize = parseInput(args, inputFile); /**return -1 if file ended**/
                break;
            case (0) :
                printf("\nshell$");
                fflush(stdout);
                argsSize = parseInput(args, NULL);
        }
        if (argsSize == -1) { // end Of File
            should_run = 0;
        }else if(argsSize != 0) { // new line (empty command) or any other non critical error, if it's = 0..
            background = checkSpecialCommands(args, argsSize);
            if(background != -1) // == -1 means that it's a history command, doesn't need to be forked executed using execv
                forkProcess(args, background, pathsSize, argsSize,splitedPaths);
        }
    }
    if(fileMode)
        fclose(inputFile);
    saveHistory();
    exit(0);
}
/**
 * Checks special commands like exit(), background commands (ended with &), and history command
 * @param args The array of strings that holds the read arguments
 * @param argsSize the number of arguments read from user (without white spaces)
 * @return 1 if background mode,
 *         0 if foregaround  mode,
 *         -1 if the command is history
 */
int checkSpecialCommands(char *args[], int argsSize) {
    int background = 0;
    if(strcmp(args[argsSize-1], "&") == 0) {
        background = 1;
        args[argsSize-1] = NULL; // For not saving the "&" in args..
    } else if(strcmp(args[0],"exit") == 0) {
        saveHistory();
        exit(0);
    } else if (strcmp(args[0],"history") == 0) {
        printHistory(stdout);
        return -1;
    }
    return background;
}

/*Takes args[] to parse the arguments
 * if background == true,
 * then the process is background (The parent waits)
 * else if background == fals,
 * then the process is foreground (The parent continues to read"from main()")
 *pathsSize contains the # paths already read
 */

void forkProcess(char *args[], int background, int pathsSize, int argsSize, char *splitedPaths[]) {
    pid_t pid;
    int status;
    /**Establish Child Handler**/
    signal(SIGCHLD, handle_sigchld);
    pid = fork();
    if(pid == 0) /*Child process*/
    {
        /* Iterate over the splittedPaths
         * Concat each path with the "Command" (args[0])
         * Then check if this executable exists.. by the end of the paths
         * check if command exists, invoke execv()
         * else print command not found*/
        if(args[0][0] == '/') {
            execv(args[0], args);
        }
        int j;
        for(j = 0; j < pathsSize; ++j) {
            char *concatedPath = concat(splitedPaths[j], args[0]);
            execv(concatedPath, args);
        }
        printf("\n");
        perror("");
        printf("\n");
        return;
    }
    else if(pid < 0) /*Error*/
    {
        perror("");
    }
    else /*Parent process*/
    {
        if(background != 1) {
            waitpid(pid, &status, 0);
        }
        /**In background mode, parent Continues to read input**/
    }

}

void handle_sigchld(int sig) {
    pid_t pid;
    int status;
    while (1) {
        pid = waitpid(-1, &status, WNOHANG);
        if (pid <= 0)/* No more zombie children to reap. */
             break;
        /* NOT REENTRANT!  Avoid in practice! */
        printf("Reaped child %d\n", pid);
    }
}
char* concat(char *s1, char *s2) {
    char *result  = malloc(strlen(s1) + strlen(s2) + 2);
    strcpy(result,s1);
    strcat(result,"/");
    strcat(result,s2);
    return result;
}
