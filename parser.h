//
// Created by basma on 13/10/16.
//

#ifndef SHELL_SCRIPT_PARSER_H
#define SHELL_SCRIPT_PARSER_H
#include <stdio.h>
                /**Constants**/
#define MAX_LINE 80 /* The maximum length command */
        /****************************/
        /* Declaration of Functions */
        /****************************/
int parseInput(char *args[], FILE *file);
int split(char *string, char *tokensArray[], char delim[]);
int findSplitedPaths (char *splitedPaths[]);
int retrieveHistory(char command[], char *args[]);
int checkSize(char *command);
#endif //SHELL_SCRIPT_PARSER_H
